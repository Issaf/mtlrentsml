# MTLRents Machine Learning Algorithm

Small algorithm used to predict Montreal rents. sklearn Gradient Boosting regressor was used. The data used was scrapped and cleaned only once as this a fun side project. It is hosted on Google cloud Engine in a flask app. The main project is in a [different repository](https://gitlab.com/Issaf/mtlrents).