# -*- coding: utf-8 -*-

# ========
# ENV VARS
# ========
env_vars = {
"GOOGLE_MAPS_API_KEY": "AIzaSyCsusevZLoUQWL9GWfjEKu-hVKdLpDajCw",
"SETTINGS_KEY": "a&tg(178)q)o8#fyv3%2%m9x@kz)7zzhop21^*douz-zrh$ajz"
}

# ============================================
# Prediction and averages calculator functions
# ============================================

def get_avg_stats(nbr_br,nbr_ba,nei):
	import pandas as pd
	import sqlite3
	import numpy as np
	import sys, os
	main_dir = os.path.dirname(__file__)
	sys.path.append(main_dir)
	conn = sqlite3.connect('data.db')
	df = pd.read_sql('SELECT Neighborhood,beds,baths,price FROM cleaned_data',conn)
	conn.close()

	mtl_avg = df[(df.beds == float(nbr_br)) & (df.baths == float(nbr_ba))].price.mean()
	nei_avg = df[(df.beds == float(nbr_br)) & (df.baths == float(nbr_ba)) & (df.Neighborhood == nei)].price.mean()
	if np.isnan(mtl_avg):
		mtl_avg = 0
	if np.isnan(nei_avg):
		nei_avg = 0

	return int(np.int64(nei_avg)), int(np.int64(mtl_avg))
	
def predictor(lat,lon,nbr_br,nbr_ba):
	import numpy as np
	import joblib
	filepath = 'regressor.pkl'
	reg = joblib.load(filepath)
	x = np.reshape([lat,lon,nbr_br,nbr_ba],(1,4))
	return int(np.int64(reg.predict(x)[0]))


# =========================================
# Geocoding and reverse geocoding functions
# =========================================

def get_coordinates(addr):
	import googlemaps
	import pandas as pd
	vars_dict = env_vars

	gm = googlemaps.Client(key= vars_dict['GOOGLE_MAPS_API_KEY'])
	req = gm.geocode(addr)[0]
	xy = req['geometry']['location']
	df = pd.DataFrame(req['address_components'])
	types = df.types.apply(' '.join).str.contains('sublocality')
	if any(types) == True: 
		nei = df[types]['long_name'].iloc[0]
	else:
		types = df.types.apply(' '.join).str.contains('locality')
		nei = df[types]['long_name'].iloc[0]			
	return xy['lat'],xy['lng'],nei

def get_address(lat,long):
	import googlemaps
	import pandas as pd
	vars_dict = env_vars

	gm = googlemaps.Client(key= vars_dict['GOOGLE_MAPS_API_KEY'])
	req = gm.reverse_geocode((lat,long))[0]
	df = pd.DataFrame(req['address_components'])
	types = df.types.apply(' '.join).str.contains('sublocality')
	if any(types) == True: 
		nei = df[types]['long_name'].iloc[0]
	else:
		types = df.types.apply(' '.join).str.contains('locality')
		nei = df[types]['long_name'].iloc[0]			
	return req['formatted_address'],nei


# ================
# Helper functions
#=================
	
def str_wrapper(str):
	return "'"+str+"'"
  
def remove_non_ascii(s):
	import string
	ascii = set(string.printable) 
	l = []
	it = filter(lambda x: x in ascii, s)
	for i in it:
		l.append(i)
	return ''.join(l)


def remove_empty_spaces(posted_time):
	return posted_time.strip('\n').lstrip(' ').rstrip(' ').rstrip('\n')
