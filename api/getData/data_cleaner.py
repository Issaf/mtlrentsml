# -*- coding: utf-8 -*-


# imports
import pandas as pd
import numpy as np
import sqlite3
import re
from datetime import datetime
from utility_functions import get_coordinates, get_address, remove_empty_spaces
import json

with open("../ENV_VARS.json") as f:
    vars_dict = json.load(f)


# Remove Duplicates
conn = sqlite3.connect('../DBs/data.db')
cur = conn.cursor()

query1 = "DELETE FROM raw_craigslist WHERE rowid NOT IN (SELECT MIN(rowid) FROM raw_craigslist GROUP BY url,price)"
query2 = "DELETE FROM raw_craigslist WHERE rowid NOT IN (SELECT MIN(rowid) FROM raw_craigslist GROUP BY address,lat,lon,beds,baths,price)"

cur.execute(query1)
cur.execute(query2)
conn.commit()



# Gle Cleaning

# 1- Check functions
def check_price(record):
	if record['price'] in range(300,5001):
		return True
	else:
		return False

def check_baths(record):
	if record['baths'] >= 1:
		return True
	else:
		return False 
		
def check_beds(record):
	if record['beds'] > 0:
		return True
	elif record['beds'] == 0:
		tl = []
		for key_word in ['studio', 'bachelor','loft','1 1/2','1.5','1&1/2']:
			if key_word in record['title'].lower(): tl.append(True)
			else: tl.append(False)
		if any(tl): return True
		else: return False
	else:
		return False 

def check_addr(record):
    cond1 = pd.isnull(record['address']) == True and pd.isnull(record['lat']) == True and pd.isnull(record['lon']) == True
    cond2 = pd.isnull(record['address']) == True and pd.isnull(record['lat']) == False and pd.isnull(record['lon']) == False
    cond3 = pd.isnull(record['address']) == False and pd.isnull(record['lat']) == True and pd.isnull(record['lon']) == True
    cond4 = pd.isnull(record['address']) == False and pd.isnull(record['lat']) == False and pd.isnull(record['lon']) == False

    if cond1:
        return False,'Case 1'
    elif cond2:
        return True,'Case 2'
    elif cond3:
        if re.match('^\d+(\s|,\s)\w+',record['address']) != None or re.match("^.+\sat\s.+$",record['address']) != None:
            return True, 'Case 3.1 & 3.2'
        else:
            return False, 'Case 3.3'
    elif cond4:
        if re.match('^\d+(\s|,\s)\w+',record['address']) != None:
            return True,'Case 4.1'
        elif re.match("^.+[^\d+]\sat\s.+[^\d+]$",record['address']) != None:
            return True, 'Case 4.2'
        elif re.match("^\D+$",record['address']) != None:
            return True, 'Case 4.4'
        else:
            return True,'Case 4.3'
                
			
# 2- Loop through each row			
df = pd.read_sql('select * from raw_craigslist',conn)

for i in range(0,df.shape[0]):
    print('At row %s' %i)
    row = df.iloc[i,:].copy()
    truth_table = [check_price(row),check_baths(row),check_beds(row),check_addr(row)[0],check_addr(row)[1]]
    if all(truth_table) == False:
        cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
        conn.commit()
    else:
        if truth_table[-1] == 'Case 2' or truth_table[-1] == 'Case 4.3' or truth_table[-1] == 'Case 4.4':
            row['address'],row['neighborhood'] = get_address(row['lat'],row['lon'])
            row['posted_time'] = datetime_object = datetime.strptime(remove_empty_spaces(row['posted_time']), '%Y-%m-%d %I:%M%p')
            cur.execute("INSERT INTO cleaned_data (url, posted_time, title, content, address, lat, lon, beds, baths, area, price, Neighborhood) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",tuple(row.iloc[1:].tolist()))
            cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
            conn.commit()
				
        elif truth_table[-1] == 'Case 3.1 & 3.2':
            try:
                row['lat'],row['lon'],row['neighborhood'] = get_coordinates(row['address'])
                row['posted_time'] = datetime_object = datetime.strptime(remove_empty_spaces(row['posted_time']), '%Y-%m-%d %I:%M%p')
                cur.execute("INSERT INTO cleaned_data (url, posted_time, title, content, address, lat, lon, beds, baths, area, price, Neighborhood) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",tuple(row.iloc[1:].tolist()))
                cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
                conn.commit()                
            except:
                cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
                conn.commit()
				
        elif truth_table[-1] == 'Case 4.1':
            try:
                row['lat'],row['lon'],row['neighborhood'] = get_coordinates(row['address'])
                row['posted_time'] = datetime_object = datetime.strptime(remove_empty_spaces(row['posted_time']), '%Y-%m-%d %I:%M%p')
                cur.execute("INSERT INTO cleaned_data (url, posted_time, title, content, address, lat, lon, beds, baths, area, price, Neighborhood) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",tuple(row.iloc[1:].tolist()))
                cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
                conn.commit()                
            except:
                cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
                conn.commit()

	
        elif truth_table[-1] == 'Case 4.2':
            try:
                addr,nei_CO = get_address(row['lat'],row['lon'])
                lat,long,nei_AD = get_coordinates(row['address'])
                if nei_AD == nei_CO:
                    row['address'] = addr
                    row['neighborhood'] = nei_CO
                    row['posted_time'] = datetime_object = datetime.strptime(remove_empty_spaces(row['posted_time']), '%Y-%m-%d %I:%M%p')
                    cur.execute("INSERT INTO cleaned_data (url, posted_time, title, content, address, lat, lon, beds, baths, area, price, Neighborhood) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",tuple(row.iloc[1:].tolist()))
                    cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
                    conn.commit()   
                else:
                    cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
                    conn.commit()
            except:
                cur.execute("DELETE FROM raw_craigslist WHERE id=?", (str(row.loc['id']),))
                conn.commit()

conn.close()