# -*- coding: utf-8 -*-

import requests
import re
from bs4 import BeautifulSoup as bsoup
import sqlite3
import time
import random
import pandas as pd


# ===================================
# Craigslist data extraction function
# ===================================

def get_data(url):
	r = requests.get(url)
	soup = bsoup(r.text,"lxml")

	# Time posted
	try:
		d = soup.find('time', class_='timeago')
		time_posted = d.text
	except:
		time_posted = None
	
	# title
	try:
		hh = soup.find('span',id='titletextonly')
		title = hh.text
	except:
		title = None
		
	# content
	try:
		ss = soup.find('section',id='postingbody')
		content = ss.text
	except:
		content = None
	
	# Address
	try:
		g = soup.find('div', class_='mapaddress')
		addr = g.text
	except:
		addr = None

	# Latitude & Longitude
	c = soup.find('div', id="map")
	try:
		lat = c['data-latitude']
		lon = c['data-longitude']
	except:
		lat = None
		lon = None

	# Bedrooms / Bathrooms / Area
	b = soup.find_all('span', class_='shared-line-bubble')
	if b != []:
		bss = b[0].text.split(' / ')
		try:
			nbr_BR = float(bss[0][:-2])
		except:
			nbr_BR = None
		try:
			nbr_BA = float(bss[1][:-2])
		except:
			nbr_BA = None			
		try:
			area = float(b[1].text[:-3])
		except:
			area = None
	else:
		nbr_BR = None
		nbr_BA = None
		area = None

	# Price
	try:
		a = soup.find('span', class_='price')
		price = int(a.string[1:])
	except:
		price = None
		
	return url, time_posted, title, content, addr, lat, lon, nbr_BR, nbr_BA, area, price



# =================
# Scrapper function
# =================

def scrapper():
	is_done = False
	main_url = "https://montreal.craigslist.ca"
	p_addition = "/search/apa?s="

	# Get the list of pages to scrape
	state = pd.read_csv('scrapping_status.csv', index_col= 0)
	p_indx = [i*120 for i in range(int(state.loc['craigslist', 'current_page']/120),18)]

	# Loop through each page
	for indx in p_indx:
		if is_done == False:
			r1 = requests.get(main_url + p_addition + str(indx))
			soup = bsoup(r1.text,'lxml')
			pages = soup.find_all('li', class_="result-row")
			i = 0
			
			# Loop through each listing
			for p in pages:
				conn = sqlite3.connect('DBs/data.db')
				cur = conn.cursor()	
				if i < len(pages):
					time.sleep(random.randint(1,4))
					url = p.a.get('href')
					if url != state.loc['craigslist', 'last_url']:
						cur.execute("INSERT OR IGNORE INTO raw_craigslist (url, posted_time, title, content, address, lat, lon, beds, baths, area, price) VALUES (?,?,?,?,?,?,?,?,?,?,?)", get_data(url))
						print('Apartment '+str(indx)+'.'+str(i)+' stored successfully')
						i += 1
						conn.commit()
					else:
						is_done = True
				conn.close()
			
			state.loc['craigslist', 'current_page'] = state.loc['craigslist', 'current_page'] + 120
			state.to_csv('scrapping_status.csv')
			time.sleep(random.randint(10,31))
		else:
			break
	
	print('Done')

	state.loc['craigslist', 'current_page'] = 0
	conn = sqlite3.connect('DBs/data.db')
	cur = conn.cursor()
	cur.execute('select url from raw_craigslist limit 1')
	state.loc['craigslist', 'last_url'] = cur.fetchone()[0]
	conn.close()

	state.to_csv('scrapping_status.csv')




scrapper()