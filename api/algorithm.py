# -*- coding: utf-8 -*-

import pandas as pd
import sqlite3
import numpy as np

## Getting data
conn = sqlite3.connect('data.db')
df = pd.read_sql('select lat,lon,beds,baths,price from cleaned_data',conn)
conn.close()

## Data preprocessing
X = df.iloc[:,:-1]
y = df.iloc[:,-1]
from sklearn.model_selection import train_test_split as tts
X_train,X_test,y_train,y_test = tts(X,y,test_size = 0.2,random_state = 53)

##===============================================================

## I) Random Forests
# from sklearn.ensemble import RandomForestRegressor as RFR
# # # Tuning part:
# # reg = RFR()
# # para = {'min_samples_leaf':[1,3,5],'n_estimators':range(100,1000,50),'min_samples_split':[2,4,6]}
# # from sklearn.model_selection import GridSearchCV
# # GSCV = GridSearchCV(estimator = reg,param_grid = para, scoring = 'mean_absolute_error',cv = 10)
# # GSCV.fit(X_train,y_train)
# # print(GSCV.best_params_)
# # y_pred = GSCV.predict(X_test)

# reg = RFR(n_estimators = 750)
# reg.fit(X_train,y_train)
# y_pred = reg.predict(X_test)
# print('Random Forest')

## II) K-Nearest Neighbors
# from sklearn.neighbors import KNeighborsRegressor as KNR
# # # tuning part:
# # reg = KNR()
# # para = {}
# # from sklearn.model_selection import GridSearchCV
# # GSCV = GridSearchCV(estimator = reg,param_grid = para, scoring = 'mean_absolute_error',cv = 10)
# # GSCV.fit(X_train,y_train)
# # print(GSCV.best_params_)
# # y_pred = GSCV.predict(X_test)

# reg = KNR(algorithm = 'brute', n_neighbors = 10, weights = 'distance')
# reg.fit(X_train,y_train)
# y_pred = reg.predict(X_test)
# print('K-Nearest Neighbors')

## III) Gradient Boosting
from sklearn.ensemble import GradientBoostingRegressor as GBR
# # Tuning part:
# reg = GBR(loss = 'huber', min_samples_split = 4, learning_rate = 0.3,max_depth = 9)
# para = {'n_estimators':range(100,1000,50)}
# from sklearn.model_selection import GridSearchCV
# GSCV = GridSearchCV(estimator = reg,param_grid = para, scoring = 'neg_mean_squared_error',cv = 10)
# GSCV.fit(X_train,y_train)
# print(GSCV.best_params_)
# y_pred = GSCV.predict(X_test)

reg = GBR(loss = 'huber', min_samples_split = 6, learning_rate = 0.1,
			max_depth = 5,n_estimators = 600,random_state = 41)
reg.fit(X_train,y_train)
y_pred = reg.predict(X_test)
print('Gradient Boosting')


## Showing results
percenterror = np.power(y_pred-y_test,2)
res_metrics = [percenterror.mean(),percenterror.median(),percenterror.quantile(q=0.95),percenterror.std()]
#res_df = res_df.append(pd.Series(res_metrics, index = ['MeanAE','MedianAE','95thAE','Std']),ignore_index = True)
print('Mean Squared Error = %s' %np.sqrt(res_metrics[0]))
print('Median Squared Error = %s' %np.sqrt(res_metrics[1]))
print('95th percentile Absolute Error = %s' %np.sqrt(res_metrics[2]))
print('Standard deviation = %s' %np.sqrt(res_metrics[3]))


## Save algorithm
import joblib
joblib.dump(reg, 'regressor.pkl')


