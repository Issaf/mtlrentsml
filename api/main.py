import os, sys
main_dir = os.path.dirname(__file__) + '/estimator'
sys.path.append(main_dir)

from flask import Flask, jsonify, request
from flask_cors import CORS

from utility_functions import get_coordinates, get_avg_stats, predictor

app = Flask(__name__)
CORS(app)

@app.route('/')
def estimate():
  address = request.args.get('address')
  bedrooms = request.args.get('bedrooms')
  bathrooms = request.args.get('bathrooms')

  lat, lon, neighborhood = get_coordinates(address)
  values = {}
  values['area_avg'], values['city_avg'] = get_avg_stats(bedrooms, bathrooms, neighborhood)
  values['estimate'] = predictor(lat, lon, bedrooms, bathrooms)
    
  return jsonify(values)
